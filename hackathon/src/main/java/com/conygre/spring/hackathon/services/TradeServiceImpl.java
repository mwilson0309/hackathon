package com.conygre.spring.hackathon.services;

import com.conygre.spring.hackathon.entities.Trade;
import com.conygre.spring.hackathon.entities.TradeState;
import com.conygre.spring.hackathon.repo.TradeRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

@Service
public class TradeServiceImpl implements TradeService {

    @Autowired
    private TradeRepository repo;

    @Override
    public Collection<Trade> getAllTrades() {
        return repo.findAll();
    }

    @Override
    public Optional<Trade> getTradesById(ObjectId id) {
        return repo.findById(id);
    }

    @Override
    public Trade addTrade(Trade trade) {
        //Setting date equal to today for the back-end
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String fullDate = df.format(date);

        //Verifying that the price and quantity is greater than 1
        if (trade.getRequestedPrice() > 0 && trade.getStockQuantity() > 0) {
            trade.setstate(TradeState.CREATED);
            trade.setDate(fullDate);
            repo.insert(trade);
            return trade;
        } else {
            System.out.println("Please enter a number greater than 0!");
            return null;
        }
    }

    @Override
    public void deleteTradeById(ObjectId id) {
        repo.deleteById(id);
    }

    @Override
    public void dropDB() {
        repo.deleteAll();
    }


}
