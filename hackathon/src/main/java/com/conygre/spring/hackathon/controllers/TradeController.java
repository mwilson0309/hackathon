package com.conygre.spring.hackathon.controllers;

import com.conygre.spring.hackathon.entities.Trade;
import com.conygre.spring.hackathon.services.TradeService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/trades")
//URL to test is localhost:8080/trades/
public class TradeController {

    @Autowired
    private TradeService service;

    @GetMapping("/ping")
    public String ping() {
        String a = "Trades service is up and running!";
        System.out.println("Inside Trades service");
        return a;
    }

    @GetMapping("/")
    public Collection<Trade> getAllTrades() {
        return service.getAllTrades();
    }

    @GetMapping("/{id}")
    public Optional<Trade> getTradesById(@PathVariable("id") ObjectId id) {
        return service.getTradesById(id);
    }

    @PostMapping("/")
    public Trade addTrade(@RequestBody Trade trade) {
        trade.setId(ObjectId.get());
        service.addTrade(trade);
        return trade;
    }

    @DeleteMapping("/{id}")
    public void deleteTradeById(@PathVariable ObjectId id) {
        service.deleteTradeById(id);
        System.out.println("Trade with ID: " + id + " deleted");
    }

    @DeleteMapping("/dropDB")
    public void dropDB() {
        service.dropDB();
        System.out.println("DB Dropped");
    }
}
