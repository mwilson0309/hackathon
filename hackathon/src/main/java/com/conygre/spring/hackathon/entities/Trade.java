package com.conygre.spring.hackathon.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {

    @Id
    private ObjectId id; //Auto-generated primary key
    private String date;
    private String stockTicker;
    private int stockQuantity;
    private double requestedPrice;
    private TradeState state;

    //Default Constructors
    public Trade() {
    }

    //Constructor
    public Trade(String sTicker, int qty, double price) {
        this.stockTicker = sTicker;
        this.stockQuantity = qty;
        this.requestedPrice = price;
    }

    public String getHexString() {
        return id.toHexString();
    }

    //Getters and Setters
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public int getStockQuantity() {
        return stockQuantity;
    }

    public void setStockQuantity(int stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    public double getRequestedPrice() {
        return requestedPrice;
    }

    public void setRequestedPrice(double requestedPrice) {
        this.requestedPrice = requestedPrice;
    }

    public TradeState getstate() {
        return state;
    }

    public void setstate(TradeState status) {
        this.state = status;
    }

    @Override
    public String toString() {
        return "Trade{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", stockTicker='" + stockTicker + '\'' +
                ", stockQuantity=" + stockQuantity +
                ", requestedPrice=" + requestedPrice +
                ", state=" + state +
                '}';
    }
}




