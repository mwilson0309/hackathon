package com.conygre.spring.hackathon.services;

import com.conygre.spring.hackathon.entities.Trade;
import org.bson.types.ObjectId;

import java.util.Collection;
import java.util.Optional;

public interface TradeService {
    Collection<Trade> getAllTrades();

    Optional<Trade> getTradesById(ObjectId id);

    Trade addTrade(Trade trade);

    void deleteTradeById(ObjectId id);

    void dropDB();
}
