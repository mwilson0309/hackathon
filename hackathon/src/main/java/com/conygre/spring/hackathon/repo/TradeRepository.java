package com.conygre.spring.hackathon.repo;

import com.conygre.spring.hackathon.entities.Trade;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeRepository extends MongoRepository<Trade, Object>{


}
