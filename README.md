# MicroService Stock Trading Platform Project
### Project by: Tim GaNun, Daniel Rocchi, Donavan Duplessis, & Mary Wilson

## Technologies and Versions Used

* Java (11.0.8-2)
* Springboot (2.4.0-SNAPSHOT)
* Gradle (0.0.1-SNAPSHOT)
* MongoDB (4.2.9 Enterprise)
* MongoDB Atlas (2020 Release)
* MongoDB Compass Community (1.21.2)
* HTML5 / CSS
* JavaScript (1.8.5)

## Project Components
### Trade REST API

* Java Springboot API built through Gradle.
* Queries the DB to get trade information.
* Posts to DB to create new trades.
* Also deletes rows in DB by ObjectID.

### TradeDB

* MongoDB using Compass Community and Atlas to connect to cloud.
* Schema: _id, date, stockTicker, stockQuantity, requestedPrice, state, _class.
* Queried by the API for GET/POST/DELETE.

### Dummy Trade Fullfillment Service

* This was provided for us by Neueda Training Instructors.
* Queries our DB to find trades that were CREATED, and changes them to PROCESSING.
* Changes all trades PROCESSING, to either FILLED or REJECTED by random chance.

### Web Frontend

* Created using HTML5, CSS, and JavaScript from scratch.
* Separated into two pages: View Trades & Make Trades.
* View Trades: Fetches results from API query to DB and displays trades dynamically.
* Make Trades: Gets information from the user using a form then sends the information to the API to add the trade to the DB. 

